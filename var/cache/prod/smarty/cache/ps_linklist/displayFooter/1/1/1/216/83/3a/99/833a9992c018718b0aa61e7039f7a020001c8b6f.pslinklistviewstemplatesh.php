<?php
/* Smarty version 3.1.32, created on 2018-08-31 17:01:33
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b894a3d6050f5_00631229',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1532446063,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5b894a3d6050f5_00631229 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?><div class="col-md-4 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Товари</p>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_69096" data-toggle="collapse">
        <span class="h3">Товари</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_69096" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/prices-drop"
                title="Our special products"
                            >
              Розпродаж
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/new-products"
                title="Нові товари"
                            >
              Новинки
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/best-sales"
                title="Наші бестселери"
                            >
              Популярні
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Інформація</p>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_93933" data-toggle="collapse">
        <span class="h3">Інформація</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_93933" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/content/1-delivery"
                title="Our terms and conditions of delivery"
                            >
              Доставка
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/content/2-legal-notice"
                title="Правова інформація"
                            >
              Правова інформація
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/content/3-terms-and-conditions-of-use"
                title="Our terms and conditions of use"
                            >
              Terms and conditions of use
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/content/4-about-us"
                title="Learn more about us"
                            >
              About us
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/content/5-secure-payment"
                title="Our secure payment method"
                            >
              Secure payment
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/contact-us"
                title="Для зв&#039;язку з нами скористайтесь контактною формою"
                            >
              Контакти
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/Мапа"
                title="Загубились? Скористайтесь пошуком"
                            >
              Мапа сайту
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://localhost:8888/prestashop/stores"
                title=""
                            >
              Магазини
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
