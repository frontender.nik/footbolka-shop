<?php
/* Smarty version 3.1.32, created on 2018-08-31 17:01:33
  from 'module:pscustomeraccountlinkspsc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b894a3d632286_36150396',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:pscustomeraccountlinkspsc',
      1 => 1532446063,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5b894a3d632286_36150396 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
<div id="block_myaccount_infos" class="col-md-2 links wrapper">
  <p class="h3 myaccount-title hidden-sm-down">
    <a class="text-uppercase" href="http://localhost:8888/prestashop/my-account" rel="nofollow">
      Ваш профіль
    </a>
  </p>
  <div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
    <span class="h3">Ваш профіль</span>
    <span class="float-xs-right">
      <span class="navbar-toggler collapse-icons">
        <i class="material-icons add">&#xE313;</i>
        <i class="material-icons remove">&#xE316;</i>
      </span>
    </span>
  </div>
  <ul class="account-list collapse" id="footer_account_list">
            <li>
          <a href="http://localhost:8888/prestashop/identity" title="Особисті дані" rel="nofollow">
            Особисті дані
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop/order-history" title="Замовлення" rel="nofollow">
            Замовлення
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop/credit-slip" title="Рахунки" rel="nofollow">
            Рахунки
          </a>
        </li>
            <li>
          <a href="http://localhost:8888/prestashop/addresses" title="Адреси" rel="nofollow">
            Адреси
          </a>
        </li>
        
	</ul>
</div>
<?php }
}
