<?php
/* Smarty version 3.1.32, created on 2018-08-31 17:31:10
  from 'module:blockreassuranceviewstemp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b89512ee07545_20933678',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9ffc009d1b66ea89054a8e253403b7d3a67d8150' => 
    array (
      0 => 'module:blockreassuranceviewstemp',
      1 => 1532446063,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5b89512ee07545_20933678 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>  <div id="block-reassurance">
    <ul>
              <li>
          <div class="block-reassurance-item">
            <img src="http://localhost:8888/prestashop/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png" alt="Політика безпеки (редагувати в модулі підтвердження клієнтів)">
            <span class="h6">Політика безпеки (редагувати в модулі підтвердження клієнтів)</span>
          </div>
        </li>
              <li>
          <div class="block-reassurance-item">
            <img src="http://localhost:8888/prestashop/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png" alt="Політика доставки (редагувати в модулі підтвердження клієнтів)">
            <span class="h6">Політика доставки (редагувати в модулі підтвердження клієнтів)</span>
          </div>
        </li>
              <li>
          <div class="block-reassurance-item">
            <img src="http://localhost:8888/prestashop/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png" alt="Політика повернення (редагувати в модулі підтвердження клієнтів)">
            <span class="h6">Політика повернення (редагувати в модулі підтвердження клієнтів)</span>
          </div>
        </li>
          </ul>
  </div>
<?php }
}
