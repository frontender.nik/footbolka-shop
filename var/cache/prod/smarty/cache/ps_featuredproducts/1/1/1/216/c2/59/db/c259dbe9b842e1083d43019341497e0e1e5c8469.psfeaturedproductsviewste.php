<?php
/* Smarty version 3.1.32, created on 2018-08-31 17:01:33
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b894a3d1c0f58_69969404',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1532446063,
      2 => 'module',
    ),
    'eb16937af03d20784919c02086835344ef2f37df' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1532446063,
      2 => 'file',
    ),
    'c30ea9ee92f0faa26cf7c0c34000fa41e9d1abca' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop/themes/classic/templates/catalog/_partials/variant-links.tpl',
      1 => 1532446063,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5b894a3d1c0f58_69969404 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Популярні товари
  </h2>
  <div class="products">
          
  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/men/1-1-hummingbird-printed-t-shirt.html#/1-rozmir-s/8-kolir-bilij" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/2-home_default/hummingbird-printed-t-shirt.jpg"
              alt = "Hummingbird printed t-shirt"
              data-full-size-image-url = "http://localhost:8888/prestashop/2-large_default/hummingbird-printed-t-shirt.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/men/1-1-hummingbird-printed-t-shirt.html#/1-rozmir-s/8-kolir-bilij">Hummingbird printed t-shirt</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Базова ціна</span>
                <span class="regular-price">28,68 ₴</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">22,94 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag discount">Ціну знижено</li>
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                      <div class="variant-links">
      <a href="http://localhost:8888/prestashop/men/1-3-hummingbird-printed-t-shirt.html#/2-rozmir-m/8-kolir-bilij"
       class="color"
       title="Білий"
              style="background-color: #ffffff"           ><span class="sr-only">Білий</span></a>
      <a href="http://localhost:8888/prestashop/men/1-2-hummingbird-printed-t-shirt.html#/1-rozmir-s/11-kolir-chornij"
       class="color"
       title="Чорний"
              style="background-color: #434A54"           ><span class="sr-only">Чорний</span></a>
    <span class="js-count count"></span>
</div>
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/na-golovnu/2-9-brown-bear-printed-sweater.html#/1-rozmir-s" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/21-home_default/brown-bear-printed-sweater.jpg"
              alt = "Brown bear printed sweater"
              data-full-size-image-url = "http://localhost:8888/prestashop/21-large_default/brown-bear-printed-sweater.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/na-golovnu/2-9-brown-bear-printed-sweater.html#/1-rozmir-s">Hummingbird printed sweater</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Базова ціна</span>
                <span class="regular-price">43,08 ₴</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">34,46 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag discount">Ціну знижено</li>
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/3-home_default/the-best-is-yet-to-come-framed-poster.jpg"
              alt = "The best is yet to come&#039; Framed poster"
              data-full-size-image-url = "http://localhost:8888/prestashop/3-large_default/the-best-is-yet-to-come-framed-poster.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm">The best is yet to come&#039;...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">34,80 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/na-golovnu/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/4-home_default/the-adventure-begins-framed-poster.jpg"
              alt = "The adventure begins Framed poster"
              data-full-size-image-url = "http://localhost:8888/prestashop/4-large_default/the-adventure-begins-framed-poster.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/na-golovnu/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm">The adventure begins Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">34,80 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/5-home_default/today-is-a-good-day-framed-poster.jpg"
              alt = "Today is a good day Framed poster"
              data-full-size-image-url = "http://localhost:8888/prestashop/5-large_default/today-is-a-good-day-framed-poster.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm">Today is a good day Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">34,80 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/home-accessories/6-mug-the-best-is-yet-to-come.html" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/6-home_default/mug-the-best-is-yet-to-come.jpg"
              alt = "Mug The best is yet to come"
              data-full-size-image-url = "http://localhost:8888/prestashop/6-large_default/mug-the-best-is-yet-to-come.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/home-accessories/6-mug-the-best-is-yet-to-come.html">Mug The best is yet to come</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">14,28 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/home-accessories/7-mug-the-adventure-begins.html" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/7-home_default/mug-the-adventure-begins.jpg"
              alt = "Mug The adventure begins"
              data-full-size-image-url = "http://localhost:8888/prestashop/7-large_default/mug-the-adventure-begins.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/home-accessories/7-mug-the-adventure-begins.html">Mug The adventure begins</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">14,28 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

          
  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost:8888/prestashop/na-golovnu/8-mug-today-is-a-good-day.html" class="thumbnail product-thumbnail">
            <img
              src = "http://localhost:8888/prestashop/8-home_default/mug-today-is-a-good-day.jpg"
              alt = "Mug Today is a good day"
              data-full-size-image-url = "http://localhost:8888/prestashop/8-large_default/mug-today-is-a-good-day.jpg"
            >
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost:8888/prestashop/na-golovnu/8-mug-today-is-a-good-day.html">Mug Today is a good day</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Ціна</span>
              <span itemprop="price" class="price">14,28 ₴</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новий</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Швидкий перегляд
          </a>
        

        
                  
      </div>

    </div>
  </article>

      </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://localhost:8888/prestashop/2-na-golovnu">
    Усі товари<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
