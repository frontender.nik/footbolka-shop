<?php return array (
  'parameters' => 
  array (
    'database_host' => 'localhost',
    'database_port' => '',
    'database_name' => 'tshirt',
    'database_user' => 'root',
    'database_password' => 'root',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'owHSQbcXoC6QGZ4a2On0mkm5GOw59SdOEMGY7p1CBtXiS2uyfvyhC2Hd',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-08-31',
    'locale' => 'uk-UA',
    'cookie_key' => '9e90ihTDcHDfbfekNjEkFQ37jBxJWEJzdYe56RoeT67gpzaS1ZiV5aY5',
    'cookie_iv' => 'XHeLejRa',
    'new_cookie_key' => 'def00000d77ac393000974e8ec6d59545929d32563400ba1254379ea2e5f8f41cc8f397eba2ad196157d5f7b240b122aed97ff7cb00f793de5c0ab84db42e8cff191bca5',
  ),
);
